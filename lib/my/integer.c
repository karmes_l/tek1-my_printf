/*
** integer.c for  in /home/karmes_l/Projets/Printf
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Nov 10 10:17:56 2014 lionel karmes
** Last update Sat Nov 15 14:45:23 2014 lionel karmes
*/

#include <stdlib.h>
#include <stdarg.h>
#include "my.h"
#include "specificators.h"

int	integer(char type)
{
  if (type == 'd' || type == 'i' || type == 'o' || type == 'u' ||
      type == 'x' || type == 'X' || type == 'b')
    return (1);
  return (0);
}

t_speci			*vaarg_short(va_list ap, t_speci *ptr_speci)
{
  unsigned short	d;
  short			c;
  char			*str2;

  if (ptr_speci->flag[3] == 'h' && integer(ptr_speci->flag[4]))
    {
      if (ptr_speci->flag[4] == 'd' || ptr_speci->flag[4] == 'i')
	{
	  c = va_arg(ap, int);
	  my_putnbr(c);
	  ptr_speci->arglen += my_strlen(int_to_str(c));
	}
      else
	{
	  d = va_arg(ap, int);
	  str2 = int_to_str_unsigned(d);
	  ptr_speci->arglen += base_unsigned(str2, ptr_speci);
	  free(str2);
	}
      ptr_speci->type = 1;
    }
  return (ptr_speci);
}

t_speci		*vaarg_long(va_list ap, t_speci *ptr_speci)
{
  unsigned long	c;
  long		d;
  char		*str2;

  if (ptr_speci->flag[3] == 'l' && integer(ptr_speci->flag[4]))
    {
      if (ptr_speci->flag[4] == 'd' || ptr_speci->flag[4] == 'i')
	{
	  d = va_arg(ap, long);
	  my_putnbr(d);
	  ptr_speci->arglen += my_strlen(int_to_str(d));
	}
      else
	{
	  c = va_arg(ap, unsigned long);
	  str2 = int_to_str_unsigned(c);
	  ptr_speci->arglen += base_unsigned(str2, ptr_speci);
	  free(str2);
	}
      ptr_speci->type = 1;
    }
  return (ptr_speci);
}

t_speci	*vaarg_char(va_list ap, t_speci *ptr_speci)
{
  if (ptr_speci->flag[4] == 'c')
    {
      my_putchar(va_arg(ap, int));
      ptr_speci->arglen += 1;
      ptr_speci->type = 1;
    }
  return (ptr_speci);
}

t_speci		*vaarg_int(va_list ap, t_speci *ptr_speci)
{
  unsigned int	c;
  int		d;
  char		*str2;

  if (integer(ptr_speci->flag[4]))
    {
      if (ptr_speci->flag[4] == 'd' || ptr_speci->flag[4] == 'i')
	{
	  d = va_arg(ap, int);
	  my_putnbr(d);
	  ptr_speci->arglen += my_strlen(int_to_str(d));
	}
      else
	{
	  c = va_arg(ap, unsigned int);
	  str2 = int_to_str_unsigned(c);
	  ptr_speci->arglen += base_unsigned(str2, ptr_speci);
	  free(str2);
	}
      ptr_speci->type = 1;
    }
  return (ptr_speci);
}
