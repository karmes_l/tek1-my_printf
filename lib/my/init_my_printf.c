/*
** my_printf.c for  in /home/karmes_l/Projets/Printf
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Nov  8 18:39:24 2014 lionel karmes
** Last update Sat Nov 15 15:19:25 2014 lionel karmes
*/

#include <stdarg.h>
#include <stdlib.h>
#include "my.h"
#include "specificators.h"

int	flag_modulo(const char *str, int i, int modulo)
{
  if (i == 0)
    modulo = 0;
  if (str[i] == '%')
      modulo++;
  else
    modulo = 0;
  return (modulo);
}

t_speci	*raz(t_speci *ptr_speci)
{
  int	i;

  i = 0;
  while (i < 5)
    {
      ptr_speci->flag[i] = 0;
      i++;
    }
  ptr_speci->len = 0;
  ptr_speci->arglen = 0;
  return (ptr_speci);
}

int	write_no_flag(int modulo, t_speci *ptr_speci, int i, const char *str)
{
  if (ptr_speci->len == 0 && modulo % 2 == 0)
    my_putchar(str[i]);
      i = i + 1 + ptr_speci->len;
  return (i);
}

t_speci	*execute_flag(t_speci *ptr_speci, const char *str, int i, va_list ap)
{
  ptr_speci = specificators(str, i + 1, ptr_speci);
  if (ptr_speci->len > 0)
    ptr_speci->arglen = format(ap, ptr_speci);
  return (ptr_speci);
}

int	nbr_char_print(int modulo, int arglen, int c)
{
  if (modulo % 2 == 1)
    c--;
  c += 1 + arglen;
  return (c);
}
