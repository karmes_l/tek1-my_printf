/*
** integer.c for  in /home/karmes_l/Projets/Printf
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Nov 10 10:17:56 2014 lionel karmes
** Last update Sat Nov 15 15:07:32 2014 lionel karmes
*/

#include <stdlib.h>
#include "my.h"
#include "specificators.h"

int	base_unsigned(char *str2, t_speci *ptr_speci)
{
  int	(*ptr_base_unsigned[4])(char *str2, t_speci *ptr_speci);
  int	i;
  int	j;

  ptr_base_unsigned[0] = &unsigned_o;
  ptr_base_unsigned[1] = &unsigned_x;
  ptr_base_unsigned[2] = &unsigned_b;
  ptr_base_unsigned[3] = &unsigned_u;
  while (i < 4)
    {
      j = ptr_base_unsigned[i](str2, ptr_speci);
      if (j > 0)
	return (j);
      i++;
    }
  return (0);
}

int		unsigned_o(char *str2, t_speci *ptr_speci)
{
  char	*str3;
  int	i;

  i = 0;
  if (ptr_speci->flag[0] == '#')
    {
      my_putchar('0');
      i++;
    }
  if (ptr_speci->flag[4] == 'o')
    {
      str3 = convert_base_unsigned(str2, "0123456789", "01234567");
      my_putstr(str3);
      i += my_strlen(str3);
      free(str3);
    }
  return (i);
}

int		unsigned_x(char *str2, t_speci *ptr_speci)
{
  char	*str3;
  int	i;

  i = 0;
  if (ptr_speci->flag[4] == 'x')
    {
      str3 = convert_base_unsigned(str2, "0123456789", "0123456789abcdef");
      i += my_strlen(str3);
      my_putstr(str3);
      free(str3);
    }
  else if (ptr_speci->flag[4] == 'X')
    {
      str3 = convert_base_unsigned(str2, "0123456789", "0123456789ABCDEF");
      i += my_strlen(str3);
      my_putstr(str3);
      free(str3);
    }
  return (i);
}

int		unsigned_b(char *str2, t_speci *ptr_speci)
{
  char	*str3;
  int	i;

  i = 0;
  if (ptr_speci->flag[4] == 'b')
    {
      str3 = convert_base_unsigned(str2, "0123456789", "01");
      my_putstr(str3);
      i += my_strlen(str3);
      free(str3);
    }
  return (i);
}

int		unsigned_u(char *str2, t_speci *ptr_speci)
{
  int	i;

  i = 0;
  if (ptr_speci->flag[4] == 'u')
    {
      my_putstr(str2);
      i += my_strlen(str2);
    }
  return (i);
}
