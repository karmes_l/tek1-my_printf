/*
** my.h for  in /home/karmes_l/Projets/Printf/include
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Fri Nov 14 14:44:08 2014 lionel karmes
** Last update Sat Nov 15 16:09:10 2014 lionel karmes
*/

#ifndef MY_H_
# define MY_H_

#include <unistd.h>
#include <stdarg.h>
#include "specificators.h"

void	my_putchar(char);
void	my_putnbr(long);
void	my_putstr(char *);
int	my_strlen(char *);
int	my_getnbr(char *);
char	*mu_strcpy(char *, char *);
char	*my_revstr(char *);
unsigned long	pow_10(int);
t_speci	*specificators(const char *str, int i, t_speci *ptr_speci);
t_speci	*find_flag(t_speci *ptr_speci, const char *str, int g, int k);
void	init_ptr(int (*ptr[])(char));
int	flag(char);
int	large(char);
int	precision(char);
int	modificator(char);
int	type(char);
int	format(va_list ap, t_speci *ptr_speci);
int	integer(char type);
t_speci	*vaarg_short(va_list ap, t_speci *ptr_speci);
t_speci	*vaarg_long(va_list ap, t_speci *ptr_speci);
t_speci	*vaarg_char(va_list ap, t_speci *ptr_speci);
t_speci	*vaarg_int(va_list ap, t_speci *ptr_speci);
t_speci	*vaarg_string(va_list ap, t_speci *ptr_spec);
t_speci	*vaarg_pointeur(va_list ap, t_speci *ptr_speci);
int	flag_S(char *str);
void	flag_S_next(char *str, int i);
char	*int_to_str(long);
char	*int_to_str_unsigned(unsigned long);
int	count_num(long nb);
int	count_num_unsigned(unsigned long nb);
char	*convert_base(char *, char *, char *);
char	*convert_base_unsigned(char *, char *, char *);
void	my_putnbr_unsigned(unsigned long);
int	power(int nb, int pow);
unsigned long	power_unsigned(unsigned long, unsigned long);
int	base_unsigned(char *str2, t_speci *ptr_speci);
int	unsigned_o(char *str2, t_speci *ptr_speci);
int	unsigned_x(char *str2, t_speci *ptr_speci);
int	unsigned_b(char *str2, t_speci *ptr_speci);
int	unsigned_u(char *str2, t_speci *ptr_speci);
int	flag_modulo(const char *str, int i, int modulo);
t_speci	*raz(t_speci *ptr_speci);
int	write_no_flag(int modulo, t_speci *ptr_speci, int i, const char *str);
t_speci *execute_flag(t_speci *ptr_speci, const char *str, int i, va_list ap);
int	nbr_char_print(int modulo, int arglen, int c);
int	my_printf(const char *str, ...);
int	my_charisnum(char c);
char	*my_strcpy(char *dest, char *src);
int	my_charisprintable(char c);

#endif /* !MY_H_ */
