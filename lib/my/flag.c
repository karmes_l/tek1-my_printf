/*
** specificator.c for  in /home/karmes_l/Projets/Printf
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun Nov  9 15:57:36 2014 lionel karmes
** Last update Fri Nov 14 14:02:17 2014 lionel karmes
*/

#include "my.h"

int	flag(char c)
{
  if (c == '-' || c == '+' || c == ' ' || c == '#')
    return (1);
  return (0);
}

int	large(char c)
{
  if (my_charisnum(c) == 0 || c == '*')
    return (1);
  return (0);
}

int	precision(char c)
{
  if (my_charisnum(c) == 0 || c == '*')
    return (1);
  return (0);
}

int	modificator(char c)
{
  if (c == 'h' || c == 'l' || c == 'L')
    return (1);
  return (0);
}

int	type(char c)
{
  char	type[16];
  int	i;

  my_strcpy(type, "diouxXfeEgGcspbS");
  i = 0;
  while (i < 16)
    {
      if (type[i] == c)
	return (1);
      i++;
    }
  return (0);
}
