/*
** my_printf.c for  in /home/karmes_l/Projets/Printf
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Nov  8 18:39:24 2014 lionel karmes
** Last update Sat Nov 15 10:18:09 2014 lionel karmes
*/

#include <stdarg.h>
#include <stdlib.h>
#include "my.h"
#include "specificators.h"

int	my_printf(const char *str, ...)
{
  int		i;
  va_list	ap;
  t_speci	*ptr_speci;
  int		modulo;
  int		c;

  i = 0;
  c = 0;
  ptr_speci = malloc(sizeof(t_speci));
  if (ptr_speci != NULL)
    {
      va_start(ap, str);
      while (str[i] != 0)
	{
	  raz(ptr_speci);
	  modulo = flag_modulo(str, i, modulo);
	  if (modulo % 2 == 1)
	    ptr_speci = execute_flag(ptr_speci, str, i, ap);
	  i = write_no_flag(modulo, ptr_speci, i, str);
	  c = nbr_char_print(modulo, ptr_speci->arglen, c);
	}
      va_end(ap);
      free(ptr_speci);
    }
  return (c);
}
