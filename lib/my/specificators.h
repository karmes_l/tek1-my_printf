/*
** specificators.h for  in /home/karmes_l/Projets/Printf/include
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Nov 12 16:56:26 2014 lionel karmes
** Last update Fri Nov 14 13:13:17 2014 lionel karmes
*/

#ifndef SPECIFICATORS_H_
# define SPECIFICATORS_H_

typedef	struct	s_speci
{
  char		flag[5];
  int		len;
  int		arglen;
  int		type;
}		t_speci;

#endif /* !SPECIFICATORS_H_ */
