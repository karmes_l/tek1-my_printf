/*
** specificators.c for  in /home/karmes_l/Projets/Printf
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun Nov  9 16:30:17 2014 lionel karmes
** Last update Fri Nov 14 20:00:05 2014 lionel karmes
*/

#include <stdlib.h>
#include "my.h"
#include "specificators.h"

t_speci		*find_flag(t_speci *ptr_speci, const char *str, int g, int k)
{
  ptr_speci->flag[g] = str[k + ptr_speci->len];
  ptr_speci->len++;
  return (ptr_speci);
}

void	init_ptr(int (*ptr[])(char))
{
  ptr[0] = &flag;
  ptr[1] = &large;
  ptr[2] = &precision;
  ptr[3] = &modificator;
  ptr[4] = &type;
}

t_speci		*specificators(const char *str, int i, t_speci *ptr_speci)
{
  int		(*ptr[5])(char);
  int		f;
  int		g;

  init_ptr(ptr);
  f = 0;
  g = 0;
  while (g < 5)
    {
      if (ptr[4](str[i + ptr_speci->len + f]))
	{
	  ptr_speci = find_flag(ptr_speci, str, 4,  i + f);
	  ptr_speci->len += f;
	  return (ptr_speci);
	}
      if (ptr[g](str[i + ptr_speci->len + f]))
	ptr_speci = find_flag(ptr_speci, str, g, i + f);
      if (str[i + ptr_speci->len] == '.' && f == 0)
	f = 1;
      g++;
    }
  ptr_speci->len = 0;
  return (ptr_speci);
}
