/*
** int_to_str.c for  in /home/karmes_l/Projets/Printf
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Nov 11 13:41:21 2014 lionel karmes
** Last update Fri Nov 14 10:27:17 2014 lionel karmes
*/

#include <stdlib.h>
#include "my.h"

char	*int_to_str(long nb)
{
  int	c;
  int	f;
  char	*str;

  c = count_num(nb);
  if (nb < 0)
    {
      c++;
      nb =-nb;
      f = 1;
    }
  str = malloc(sizeof(char) * (c + 1));
  if (str != NULL)
    {
      str[c] = 0;
      while (c > 0)
	{
	  str[c - 1] = 48 + nb % 10;
	  nb /= 10;
	  c--;
	}
      if (f == 1)
	str[0] = '-';
    }
  return (str);
}
