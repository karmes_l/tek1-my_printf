/*
** my_put_nbr.c for my_put_nbr in /home/karmes_l/test/tmp_Piscine_C_J03
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Oct  1 16:28:28 2014 lionel karmes
** Last update Sat Nov 15 15:52:23 2014 lionel karmes
*/

#include "my.h"

void	my_putnbr_unsigned(unsigned long nb)
{
  int	total_char;
  int	i;
  int	c_all;

  total_char = count_num_unsigned(nb);
  i = total_char;
  while (i > 0)
    {
      c_all =  (int) (nb / pow_10(i - 1));
      my_putchar(48 + c_all);
      nb = nb - c_all * pow_10(i-1);
      i = i - 1;
    }
}
