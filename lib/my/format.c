/*
** format.c for  in /home/karmes_l/Projets/Printf
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun Nov  9 16:59:00 2014 lionel karmes
** Last update Fri Nov 14 13:33:26 2014 lionel karmes
*/

#include <stdarg.h>
#include "my.h"
#include "specificators.h"

int		format(va_list ap, t_speci *ptr_speci)
{
  t_speci	*(*ptr[6])(va_list ap, t_speci *ptr_speci);
  int	i;
  int	f;

  i = 0;
  ptr[0] = &vaarg_short;
  ptr[1] = &vaarg_long;
  ptr[2] = &vaarg_int;
  ptr[3] = &vaarg_char;
  ptr[4] = &vaarg_string;
  ptr[5] = &vaarg_pointeur;
  f = 0;
  ptr_speci->type = 0;
  while (i < 6 && f == 0)
    {
      ptr_speci = ptr[i](ap, ptr_speci);
      f = ptr_speci->type;
      i++;
    }
  return (ptr_speci->arglen);
}
