/*
** other.c for  in /home/karmes_l/Projets/Printf
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Nov 10 19:35:52 2014 lionel karmes
** Last update Sat Nov 15 16:28:31 2014 lionel karmes
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "my.h"

void	flag_S_next(char *str, int i)
{
  char *c;
  char *str3;

  my_putstr("\\");
  if (str[i] > 126)
    my_putchar('0');
  if (str[i] < 8)
    my_putchar('0');
  c = int_to_str(str[i]);
  str3 = convert_base(c, "0123456789", "01234567");
  free(c);
  my_putstr(str3);
  free(str3);
}

int	flag_S(char *str) /* 26 ligne ! */
{
  int	i;
  int	len_octal;

  i = 0;
  len_octal = 0;
  while (str[i] != 0)
    {
      if (my_charisprintable(str[i]) == 1)
	{
	  flag_S_next(str, i);
	  len_octal += 3;
	}
      else
	my_putchar(str[i]);
      i++;
    }
  return (len_octal + i);
}

t_speci	*vaarg_string(va_list ap, t_speci *ptr_speci)
{
  char	*str;

  if (ptr_speci->flag[4] == 's' || ptr_speci->flag[4] == 'S')
    {
      str = va_arg(ap, char *);
      if (str == NULL)
	{
	  my_putstr("(null)");
	  ptr_speci->arglen += 6;
	}
      else if (ptr_speci->flag[4] == 'S')
      	ptr_speci->arglen += flag_S(str);
      else
	{
	  my_putstr(str);
	  ptr_speci->arglen += my_strlen(str);
	}
      ptr_speci->type = 1;
    }
  return (ptr_speci);
}

t_speci		*vaarg_pointeur(va_list ap, t_speci *ptr_speci)
{
  unsigned long	c;
  char		*str;
  char		*str3;

  if (ptr_speci->flag[4] == 'p')
    {
      if ((c = (unsigned long) va_arg(ap, void *)) == 0)
      	{
      	  my_putstr("(nil)");
      	  ptr_speci->arglen += 5;
      	}
      else
	{
	  str = int_to_str_unsigned(c);
	  str3 = convert_base_unsigned(str, "0123456789", "0123456789abcdef");
	  free(str);
	  my_putstr("0x");
	  my_putstr(str3);
	  ptr_speci->arglen = my_strlen(str3);
	  free(str3);
	}
      ptr_speci->type = 1;
    }
  return (ptr_speci);
}
